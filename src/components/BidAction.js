import { Row, Col, InputNumber, Button } from 'antd';
import { DollarTwoTone } from '@ant-design/icons';

function BidAction({
  isBidClosed = false,
  minBid = 0,
  closingBid = 0,
  onBidChange,
  onSubmitBid,
  bidAmount,
}) {
  const min = isBidClosed ? closingBid : minBid;
  return (
    <Row gutter={16}>
      <Col>
        <DollarTwoTone style={{ fontSize: 40 }} />
      </Col>
      <Col>
        <InputNumber
          disabled={isBidClosed}
          size='large'
          min={min}
          value={bidAmount}
          onChange={onBidChange}
        />
      </Col>
      {!isBidClosed && (
        <Col>
          <Button type='primary' size='large' onClick={onSubmitBid}>
            Add Bid
          </Button>
        </Col>
      )}
    </Row>
  );
}

export default BidAction;
