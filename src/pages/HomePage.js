import { useEffect, useState } from 'react';
import { Row, Col, Input, Typography, Divider, Pagination } from 'antd';

import ProductListItem from '../components/ProductListItem';
import { useAuth } from '../hooks/useAuth';

const { Title, Text } = Typography;

function HomePage() {
  const [search, setSearch] = useState('');
  const [products, setProducts] = useState([]);
  const [pagination, setPagination] = useState({
    current_page: 1,
    per_page: 10,
    total: 0,
  });

  const auth = useAuth();
  const { token } = auth.user;

  useEffect(() => {
    fetch(
      `http://localhost:8000/api/products?page=${pagination.current_page}&searchTerm=${search}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      }
    )
      .then((res) => res.json())
      .then(({ data, ...paginationData }) => {
        setProducts(data);
        setPagination(paginationData);
      });
  }, [pagination.current_page, search, token]);

  const onSearch = (value) => {
    setSearch(value);
  };

  const onPaginationChange = (page, pageSize) => {
    setPagination({ ...pagination, current_page: page, per_page: pageSize });
  };

  return (
    <div>
      <Divider />
      <Row gutter={16} style={{ paddingTop: '16px', paddingBottom: '32px' }}>
        <Col span={12}>
          <Title>Available Items</Title>
        </Col>
        <Col span={12}>
          <Text type='secondary'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </Text>
        </Col>
      </Row>
      <Row gutter={16} style={{ paddingTop: '32px', paddingBottom: '16px' }}>
        <Col span={16}>
          <Input.Search
            placeholder='Search by name and description'
            allowClear
            onSearch={onSearch}
          />
        </Col>
      </Row>
      <Row gutter={[16, 16]} style={{ padding: '32px 0px' }}>
        {products.map((product) => (
          <Col key={product.id} span={8}>
            <ProductListItem {...product} />
          </Col>
        ))}
      </Row>
      <Row gutter={16}>
        <Col offset={6} span={16}>
          <Pagination
            current={pagination.current_page}
            pageSize={pagination.per_page}
            total={pagination.total}
            onChange={onPaginationChange}
            showTotal={(total, range) =>
              `${range[0]}-${range[1]} of ${total} items`
            }
          />
        </Col>
      </Row>
    </div>
  );
}

export default HomePage;
