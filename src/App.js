import './App.css';

import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import ProductPage from './pages/ProductPage';
import SettingsPage from './pages/SettingsPage';

import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
  Link,
} from 'react-router-dom';
import { Layout, Menu, Dropdown, Avatar, Row, Col, Button } from 'antd';
import {
  UserOutlined,
  LogoutOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { ProvideAuth, useAuth } from './hooks/useAuth';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { makeServer } from './mockServer';

if (process.env.NODE_ENV === 'development' && process.env.USE_MOCK_SERVER) {
  makeServer({ environment: 'development' });
}

const { Header, Content, Footer } = Layout;

function AuthedRoute({ children, ...rest }) {
  const auth = useAuth();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth.user ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

function UserMenu() {
  const history = useHistory();
  const auth = useAuth();
  return (
    <Menu>
      <Menu.Item icon={<SettingOutlined />} key='settings'>
        <Link to='/settings'>Settings</Link>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item icon={<LogoutOutlined />} key='logout'>
        <Button
          type='text'
          onClick={() => {
            auth.logout(() => {
              localStorage.removeItem('user');
              history.push('/');
            });
          }}
        >
          Logout
        </Button>
      </Menu.Item>
    </Menu>
  );
}

function UserPanel() {
  const auth = useAuth();
  if (!auth.user) return null;

  console.log(auth.user);
  return (
    <Menu.Item key='user' style={{ float: 'right' }}>
      <Dropdown overlay={<UserMenu />} trigger={['hover']}>
        <Row gutter={16}>
          <Col>
            <Avatar
              style={{ backgroundColor: '#87d068' }}
              icon={<UserOutlined />}
              shape='square'
            />
          </Col>
          <Col>{auth.user.email}</Col>
        </Row>
      </Dropdown>
    </Menu.Item>
  );
}

function App() {
  return (
    <ProvideAuth>
      <Router>
        <Layout className='layout'>
          <Header>
            <div className='logo' />
            <Menu theme='dark' mode='horizontal'>
              <Menu.Item key='home'>
                <Link to='/'>Home</Link>
              </Menu.Item>
              <Menu.Item key='settings'>
                <Link to='/settings'>Settings</Link>
              </Menu.Item>
              <UserPanel />
            </Menu>
          </Header>
          <Content style={{ padding: '24px 32px' }}>
            <div className='site-layout-content'>
              <Switch>
                <Route path='/login'>
                  <LoginPage />
                </Route>
                <AuthedRoute exact path='/'>
                  <HomePage />
                </AuthedRoute>
                <AuthedRoute path='/products/:id'>
                  <ProductPage />
                </AuthedRoute>
                <AuthedRoute path='/settings'>
                  <SettingsPage />
                </AuthedRoute>
              </Switch>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            © {new Date().getFullYear()}
          </Footer>
        </Layout>
      </Router>
    </ProvideAuth>
  );
}

export default App;
